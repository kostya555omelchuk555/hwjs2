let name = '';
let age = '';

while (name === '' || isNaN(age) || age === '') {
  name = prompt('Please enter your name', name);
  age = prompt('Please enter your age', age);
}

if (age < 18) {
  alert('You are not allowed to visit this website.');
} else if (age >= 18 && age <= 22) {
  const confirmed = confirm('Are you sure you want to continue?');
  if (confirmed) {
    alert(`Welcome, ${name}!`);
  } else {
    alert('You are not allowed to visit this website.');
  }
}
else if (age > 22) {
	alert(`Welcome, ${name}!`);
}
else {
  alert(`Welcome, ${name}!`);
}